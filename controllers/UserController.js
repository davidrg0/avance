const io = require('../io.js');
//hash
const argon2 = require('argon2');
const uuid = require('uuid/v1');
//mLab
const requestJson = require('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/techu/collections/";
const mLabAPIKey = "apiKey=JyUQEWXlucUXNeWullzXEZd8sVojrSpI";
//modules
const crypt = require('../crypt.js');


function getUsersV1(req, res){
  console.log("GET /apitechu/v1/users");

  var users = require('../usuarios.json');
  var respuesta = users;

  if(typeof req.query.$top !== 'undefined'){
    var respuesta=users.slice(0,req.query.$top);
  }

  if(typeof req.query.$count !== 'undefined' && req.query.$count == "true"){
    respuesta.push({"count" : users.length});
  }

  res.send(respuesta);
}

function getUsersV2(req, res){
  console.log("GET /apitechu/v2/users");

  httpClient = requestJson.createClient(baseMLabURL);
  console.log("Cient created");
  httpClient.get("user?" + mLabAPIKey,
    function(err,resMLAb,body){
      var response = !err ? body : {
        "msg" : "Error obteniendo usuarios"
      }
      res.send(response);
    }
  )
}

function getUserByIdV2(req, res){
  console.log("GET /apitechu/v2/users/:id");

  var id = req.params.id;
  var query = 'q={"id" : ' + id + '}';

  httpClient = requestJson.createClient(baseMLabURL);
  console.log("Cient created");
  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err,resMLAb,body){
      if (err) {
        var response = {
          "msg" : "Error obteniendo usuario"
        }
        res.status(500);
      } else {
        if (body.length>0) {
          var  response = body[0];
        } else {
          var response = {
            "msg" : "Usuario no encontrado"
          }
          res.status(404);
        }
      }
      res.send(response);
    }
  )
}

function getUserAccountsV2(req, res){
  console.log("GET /apitechu/v2/users/:id/accounts");

  var id = req.params.id;
  var query = 'q={"userId" : ' + id + '}';

  httpClient = requestJson.createClient(baseMLabURL);
  console.log("Cient created");
  httpClient.get("cuenta?" + query + "&" + mLabAPIKey,
    function(err,resMLAb,body){
      if (err) {
        var response = {
          "msg" : "Error obteniendo cuentas"
        }
        res.status(500);
      } else {
        if (body.length>0) {
          var  response = body;
        } else {
          var response = {
            "msg" : "Cuenta no encontrado"
          }
          res.status(404);
        }
      }
      res.send(response);
    }
  )
}


function createUserV1(req, res){
  console.log("POST /apitechu/v1/users");
  console.log("first_name es " + req.body.first_name);
  console.log("last_name es " + req.body.last_name);
  console.log("email es " + req.body.email);
  console.log("password es " + req.body.password);

  argon2.hash(req.body.password, {type: argon2.argon2id}).then(hash => {
    console.log('el hash a argon2 es: ' + hash);
    var newUser = {
      "id" : uuid(),
      "first_name" : req.body.first_name,
      "last_name" : req.body.last_name,
      "email" : req.body.email,
      "password" : hash
    };
    var users = require('../usuarios.json');
    users.push(newUser);
    io.writeUserDateToFile(users);
    console.log("Usuario añadido con éxto");
    res.send({"msg" : "Usuario añadido con éxto"});
  }).catch(err => {
    console.log("Algo ha ido mal :( " + err);
    res.send({"msg" : "No se ha podido crear el usuario"});
  });
}

function deleteUserV1(req, res){
  console.log("DELETE /apitechu/v1/users/:id");
  console.log("id es " + req.params.id);

  var users = require('../usuarios.json');
  var deleteOK = false;
  for (let user of users) {
    if(user.id == req.params.id){
      var index = users.indexOf(user);
      users.splice(index,1);
      io.writeUserDateToFile(users);
      console.log("Usuario borrado " + JSON.stringify(user));
      res.send({"msg" : "Usuario borrado"});
      deleteOK = true;
      break;  //Asuminos que solo hay 1 objeto con ese id
    }
  }
  if (!deleteOK){
    console.log("El usuario no existe!");
    res.status(404).send({"msg" : "El usuario no existe!"});
  }
}

function createUserV2(req, res){
  console.log("POST /apitechu/v2/users");
  console.log("first_name es " + req.body.first_name);
  console.log("last_name es " + req.body.last_name);
  console.log("email es " + req.body.email);
  //console.log("password es " + req.body.password);

  argon2.hash(req.body.password, {type: argon2.argon2id}).then(hash => {
    console.log('el hash a argon2 es: ' + hash);
    var newUser = {
      "id" : 50,
      "first_name" : req.body.first_name,
      "last_name" : req.body.last_name,
      "email" : req.body.email,
      "password" : hash
    };
    httpClient = requestJson.createClient(baseMLabURL);
    httpClient.post("user?" + mLabAPIKey, newUser,
      function(err,resMLAb,body){
        var response = err ? body : {
          "msg" : "Usuario creando"
        }
        res.send(response);
      }
    )
  }).catch(err => {
    console.log("Algo ha ido mal :( " + err);
    res.send({"msg" : "No se ha podido crear el usuario"});
  });
}



//v1
module.exports.getUsersV1 = getUsersV1;
module.exports.createUserV1 = createUserV1;
module.exports.deleteUserV1 = deleteUserV1;
//v2
module.exports.getUsersV2 = getUsersV2;
module.exports.getUserAccountsV2 = getUserAccountsV2;
module.exports.getUserByIdV2 = getUserByIdV2;
module.exports.createUserV2 = createUserV2;
