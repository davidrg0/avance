const io = require('../io.js');
const bodyParser = require('body-parser');

//auth y hash
const argon2 = require('argon2');
const uuid = require('uuid/v1');
const jwt = require('jsonwebtoken');
// const bearerAuthorization = require('express-authorization-bearer');
//mLab
const requestJson = require('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/techu/collections/";
const mLabAPIKey = "apiKey=JyUQEWXlucUXNeWullzXEZd8sVojrSpI";
//modules
const crypt = require('../crypt.js');

function loginV1(req, res){
  console.log("POST /apitechu/v1/login");

  var users = require('../usuarios.json');
  for (user of users) {
    if(user.email == req.body.email){
      argon2.verify(user.password, req.body.password).then(match => {
        if (match) {
          user.logged = true;
          io.writeUserDateToFile(users);
          var authHeader = "Bearer " + jwt.sign({ "id" : user.id, "rol" : "ususario"}, 'secret', { expiresIn : '1h' });
          res.setHeader('Authorization', authHeader);
          res.send({"mensaje" : "Login correcto", "idUsuario" : user.id});
        } else {
          res.send({"mensaje" : "Login incorrecto"});
        }
      }).catch(err => {
        console.log("Algo ha ido mal :( " +  err);
      });
    }
  }
}

function logoutV1(req, res){
  console.log("POST /apitechu/v1/logout");

  var users = require('../usuarios.json');
  for (user of users) {
    if(user.id == req.body.id && user.logged == true){
      delete user.logged;
      io.writeUserDateToFile(users);
      res.send({"mensaje" : "Logout correcto", "idUsuario" : user.id});
      break; // No necesario porque el res.send sale de la funcion
    }
  }
  res.send({"mensaje" : "Logout incorrecto"});
}

function loginV2(req, res){
  console.log("POST /apitechu/v2/login");
  var pass = req.body.password;
  var email = req.body.email;
  var query = 'q={"email" : "' + email + '"}';
  httpClient = requestJson.createClient(baseMLabURL);
  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err,resMLAb,body){
      if (!err && body.length>0) {
        var hashedpass = body[0].password;
        argon2.verify(hashedpass, pass).then(match => {
          if (match) {
            console.log("COINCIDE!!!!");
            var putBody = '{"$set":{"logged":true}}';
            httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
              function(putErr,putMLAb,putBody){
                //nothing to do
              }
            )
            var authHeader = "Bearer " + jwt.sign({ "id" : body[0].id}, 'secret', { expiresIn : '1h' });
            res.setHeader('Authorization', authHeader);
            res.send({"mensaje" : "Login correcto", "idUsuario" : body[0].id});
          } else {
            console.log("NO COINCIDE!!!!");
            res.status(404);
            res.send({"mensaje" : "Login incorrecto"});
          }
        }).catch(err => {
          console.log("Algo ha ido mal :( " +  err);
        });
      } else{
        res.status(404);
        res.send({"mensaje" : "Error accediendo a la base de datos"});
      }
    })
}


function logoutV2(req, res){
  console.log("POST /apitechu/v2/logout/:id");

  userId = req.params.id
  var query = 'q={"id" : ' + userId + '}';
  httpClient = requestJson.createClient(baseMLabURL);
  console.log("Cient created");
  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err,resMLAb,body){
      console.log(body.length)
      if (err) {
        var response = {
          "msg" : "Error obteniendo usuario"
        }
        res.status(500);
        res.send(response);
      } else {
        if (body.length>0 && body[0].logged) {
          console.log("Usuario logado")
          var putBody = '{"$unset":{"logged":""}}';
          httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
            function(putErr,putMLAb,putBody){
              var response = {
                "mensaje" : "Logout correcto",
                "idUsuario" : userId
              }
              res.send(response);
            }
          )
          } else {
          console.log("usuario no logado")
          var response = {
            "msg" : "Logout incorrecto"
          }
          res.status(404)
          res.send(response);
        }
      }
  })
}



//v1
module.exports.loginV1 = loginV1;
module.exports.logoutV1 = logoutV1;

//v2
module.exports.loginV2 = loginV2;
module.exports.logoutV2 = logoutV2;
