function helloV1(req, res){
  console.log("GET /apitechu/v1/hello");

  res.send({"msg" : "Hola desde API TechU Autobuilt and auto-deployed!"});
}

function monsterV1(req, res){
  console.log("POST /apitechu/v1/monstruo/:p1/:p2");

  console.log("Parametros");
  console.log(req.params);

  console.log("Query string");
  console.log(req.query);

  console.log("Headers");
  console.log(req.headers);

  console.log("Body");
  console.log(req.body);

  res.send({"msg" : "Ya está"});
}

module.exports.helloV1 = helloV1;
module.exports.monsterV1 = monsterV1;
