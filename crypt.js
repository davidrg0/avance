const bcrypt = require('bcrypt');

function hash(data){
  console.log("Hasing data");
  return bcrypt.hashSync(data,10);
}

function checkPassword(sentPassword, userHash){
  console.log("Checkig password");
  return bcrypt.compareSync(sentPassword, userHash);
}

module.exports.hash = hash
module.exports.checkPassword = checkPassword
