//server: express
const express = require('express');
const app = express();
const port = process.env.PORT || 3000;
const bodyParser = require('body-parser');

app.use(bodyParser.json());
app.listen(port);
console.log("API escuchando en el puerto " + port);

//controllers
const userController = require ('./controllers/UserController.js');
const authController = require ('./controllers/AuthController.js');
const dummyController = require ('./controllers/DummyController.js');

//users v1
app.get('/apitechu/v1/users',userController.getUsersV1);
app.post('/apitechu/v1/users',userController.createUserV1);
app.delete('/apitechu/v1/users/:id',userController.deleteUserV1);

//users v2
app.get('/apitechu/v2/users',userController.getUsersV2);
app.get('/apitechu/v2/users/:id/accounts',userController.getUserAccountsV2);
app.get('/apitechu/v2/users/:id',userController.getUserByIdV2);
app.post('/apitechu/v2/users',userController.createUserV2);


//auth v1
app.post('/apitechu/v1/login',authController.loginV1);
app.post('/apitechu/v1/logout',authController.logoutV1);

//auth v2
app.post('/apitechu/v2/login',authController.loginV2);
app.post('/apitechu/v2/logout/:id',authController.logoutV2);

//dummy (API testing)
app.get('/apitechu/v1/hello',dummyController.helloV1);
app.post('/apitechu/v1/monstruo/:p1/:p2',dummyController.monsterV1);
